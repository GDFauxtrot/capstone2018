using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class #SCRIPTNAME# : WeaponBase {

    // See WeaponBase.cs for all functionality and base class implementations!


    // Fire modes (mandatory override)

    public override void FirePrimary() {
        #NOTRIM#
    }

    public override void FireSecondary() {
        #NOTRIM#
    }

    // WeaponBase's Unity base functionality

    protected override void Awake() {
        base.Awake();
    }

    protected override void Update() {
        base.Update();
    }

}
