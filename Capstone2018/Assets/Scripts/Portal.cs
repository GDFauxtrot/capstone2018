﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Note to self - Portal's distance from plane before teleport occurs is roughly 0.0006 Unity units (~52 Source units = 1 Unity unit (1 meter))
// We aren't necessarily copying Portal 1-to-1 here but some approximate behavior would be ideal

public class Portal : MonoBehaviour {

    public Camera playerCamera;

    Camera[] portalCameras;
    Camera portalCamera;
    public RenderTexture portalTexture;

    public Portal attachedPortal;

    bool playerInFrontOfPortal;

    List<Vector3> meshVerts;

    public bool farEnoughFromFrame;

    void Awake() {
        // Set up a model portal camera
        portalCamera = Instantiate(playerCamera == null ? Camera.main : playerCamera, transform.position, playerCamera.transform.rotation);

        portalCamera.cullingMask = ~0; // Everything

        foreach (Transform child in portalCamera.transform)
            DestroyImmediate(child.gameObject); // Camera has children? kill them

        // portalCamera.transform.RotateAround(portalCamera.transform.position, Vector3.up, 180);

        portalTexture = new RenderTexture(Screen.width, Screen.height, 24);
        portalCamera.depth = playerCamera.depth;
        portalCamera.targetTexture = portalTexture;

        // portalCamera.clearStencilAfterLightingPass = false;

        portalCameras = new Camera[Constants.MAX_PORTALS_PER_FRAME];

        // portalCamera.clearFlags = CameraClearFlags.Nothing;

        for (int i = 0; i < Constants.MAX_PORTALS_PER_FRAME; ++i) {
            portalCameras[i] = Instantiate(portalCamera);
            portalCameras[i].depth = --portalCamera.depth; // Last portal drawn first (front to back, main cam being last)
            portalCameras[i].targetTexture = portalTexture; // They all write to the same texture, portal frame acting as a hole

            portalCameras[i].gameObject.name = gameObject.name + "RecursionCam" + i.ToString();
        }

        Destroy(portalCamera.gameObject);

        meshVerts = new List<Vector3>();
    }

    void Update() {
        if (!attachedPortal)
            return;
        
        // Helpful bool for figuring out if player is in front of portal, used later
        float distFromFrontOfPortal = Vector3.Dot((playerCamera.transform.position - transform.position).normalized, -transform.forward);
        playerInFrontOfPortal = distFromFrontOfPortal > 0;
        // Dist check to portal frame - if we're close enough to not care about culling, then don't do custom projection stuff.
        attachedPortal.farEnoughFromFrame = distFromFrontOfPortal > 0.001f;

        // Get vector between other portal cam and player cam, reflected against the surface
        Vector3 reflect = playerCamera.transform.position - attachedPortal.transform.position;
        reflect = Vector3.Reflect(reflect, attachedPortal.transform.up);
        reflect = Vector3.Reflect(reflect, attachedPortal.transform.forward);

        // Loop will go through and apply wonderful (confusing) maths to get portals set up.
        // All we need is a pos and rot - starts here with this portal
        Vector3 portalPos = transform.position;
        Quaternion portalRot = transform.rotation;

        Quaternion rotDifference = transform.rotation * Quaternion.Inverse(attachedPortal.transform.rotation);
        Quaternion rotDifferenceOrig = rotDifference;
        Vector3 posDifference = attachedPortal.transform.position - transform.position;

        bool test = Input.GetKeyDown(KeyCode.T);

        // GLDraw.DrawRay(transform.position, posDifference, Color.green);
        for (int i = 0; i < Constants.MAX_PORTALS_PER_FRAME; ++i) {
            if (test)
                portalCameras[i].enabled = !portalCameras[i].enabled;

            // Calculate rotational difference between this portal and the other
            
            Vector3 thisReflect = rotDifference * reflect;

            // Player FOV changes, have to copy it
            portalCameras[i].fieldOfView = playerCamera.fieldOfView;

            // if (i == 0) {
                // First camera has a particular pos+rot setup, other cameras get it easier

                // Add rotational difference to camera
                portalCameras[i].transform.position = portalPos + thisReflect;

                Quaternion rot = Quaternion.Inverse(rotDifferenceOrig) * Quaternion.Euler(Vector3.up * 180);

                // Rotate camera to correct direction (player cam rotated 180 degrees + rot difference)
                portalCameras[i].transform.rotation = playerCamera.transform.rotation;
                portalCameras[i].transform.RotateAround(portalCameras[i].transform.position, Vector3.up, 180);
                portalCameras[i].transform.rotation = rotDifference * portalCameras[i].transform.rotation;

                rotDifference *= Quaternion.Inverse(rot);
                // Fix portalPos and portalRot to be ahead (next spot)
                
                // Debug.Log(gameObject.name + " " + posDifference + " " + rot.eulerAngles);
                posDifference = Quaternion.Inverse(rot) * posDifference;
                // Debug.Log(gameObject.name + " " + posDifference);
                // GLDraw.DrawRay(portalPos, -posDifference, Color.blue);

                portalRot *= Quaternion.Inverse(rot);
                portalPos -= posDifference;
            // } else {
                
                // Figure out position of next portal
                //  * Create fake GO with transform and rot of portal
                //  * Rotate the GO by the difference between portals
                //  * Move it backward by the distance between portals

                /*
                // Find the rotational difference between the portals, but this time with a 180 degree flip. This feeds directly into the cameras' rotations.

                // Rotating a Quaternion on only one euler axis is weird, so time to hack
                Quaternion localRightFlip = flipQuaternionOnLocalRightAxis(transform.rotation);

                Quaternion camRotDifference = attachedPortal.transform.rotation * Quaternion.Inverse(localRightFlip);

                Debug.Log(camRotDifference + " " + camRotDifference.eulerAngles);
                // Rotate this camera and then move it back some amt determined by portal distance
                portalCameras[i].transform.rotation = transform.rotation * Quaternion.SlerpUnclamped(Quaternion.identity, Quaternion.Inverse(camRotDifference), i);
                portalCameras[i].transform.position = portalCameras[i-1].transform.position;
                portalCameras[i].transform.position -= portalCameras[i].transform.forward * Vector3.Distance(transform.position, attachedPortal.transform.position);
                // portalCameras[i].transform.rotation = portalCameras[0].transform.rotation;
                // portalCameras[i].transform.rotation = 
                */
            // }

            // If too close to portal frame, the custom projection matrix that culls objects also destroys the depth
            // buffer due to lack of precision (i dunno how to fix in the matrix) and things get a bit out of wack.
            if (farEnoughFromFrame) {
                // Set up oblique view frustum to clip most of the scene
                // (this culls objects between cam and portal and also cuts down on rendering, woo)

                foreach (Vector3 v3 in GetComponent<MeshFilter>().mesh.vertices) {
                    meshVerts.Add(transform.TransformPoint(v3));
                }

                Plane planeMeshPlane = new Plane(meshVerts[0], meshVerts[2], meshVerts[3]);
                planeMeshPlane.normal = -planeMeshPlane.normal;
                Vector4 clipPlaneWorldSpace = new Vector4(planeMeshPlane.normal.x, planeMeshPlane.normal.y, planeMeshPlane.normal.z, -Vector3.Dot(planeMeshPlane.normal, transform.position));
                Vector4 clipPlaneCamSpace = Matrix4x4.Transpose(portalCameras[i].cameraToWorldMatrix) * clipPlaneWorldSpace;
                portalCameras[i].projectionMatrix = portalCameras[i].CalculateObliqueMatrix(clipPlaneCamSpace);
                
                // Projection matrix FOV is 90. To modify, projection matrix, [0][0] and [1][1] must be adjusted by a 'focal length'.

                // [0][0] = Screen.height/Screen.width * focal
                // [1][1] = focal
                portalCameras[i].projectionMatrix = new Matrix4x4(new Vector4(
                        (Screen.height/(float)Screen.width) * projectionFOVToFocalLength(playerCamera.fieldOfView),
                        portalCameras[i].projectionMatrix.m10,
                        portalCameras[i].projectionMatrix.m20,
                        portalCameras[i].projectionMatrix.m30
                    ), new Vector4(
                        portalCameras[i].projectionMatrix.m01,
                        projectionFOVToFocalLength(playerCamera.fieldOfView),
                        portalCameras[i].projectionMatrix.m21,
                        portalCameras[i].projectionMatrix.m31
                    ),
                    portalCameras[i].projectionMatrix.GetColumn(2), portalCameras[i].projectionMatrix.GetColumn(3));

                meshVerts.Clear();
            } else {
                portalCameras[i].ResetProjectionMatrix();
            }
        }

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetTexture("_MainTex", attachedPortal.portalTexture);
        GetComponent<MeshRenderer>().SetPropertyBlock(block);
    }

    /// <summary>
    /// Converts the horizontal FOV of the screen to a focal length, used for the custom projection matrix of portal cameras.
    /// </summary>
    private float projectionFOVToFocalLength(float FOV) {
        return 1 / Mathf.Tan(FOV / (2 * Mathf.Rad2Deg) );
    }

    /// <summary>
    /// Rotates a Quaternion on its local right axis by 180 degrees. Just a hack to get around doing two Quaternion operations.
    /// </summary>
    private Quaternion flipQuaternionOnLocalRightAxis(Quaternion inQuat) {
        float qX = inQuat.x;
        float qY = inQuat.y;
        float qZ = inQuat.z;
        float qW = inQuat.w;
        inQuat.x = -qW;
        inQuat.y = -qZ;
        inQuat.z = qY;
        inQuat.w = qX;
        
        return inQuat;
    }
}
