﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class WeaponBase : MonoBehaviour {

    /// <summary>
    /// If enabled, disable weapon pick up and related behavior for this weapon.
    /// </summary>
    public bool heldByPlayer;

    public string weaponName = "Weapon";
    public WeaponType weaponTypePrimary = WeaponType.Hitscan, weaponTypeSecondary = WeaponType.None;

    // Primary fire
    public GameObject fire0Projectile = null;
    public int fire0Damage = 0;
    public int fire0DamageRandomRange = 0;
    public int fire0CurrentAmmo = 0;
    public int fire0AmmoMax = 0;
    public int fire0AmmoPerClip = -1;
    public float fire0MaxSpread = 0;
    public AnimationCurve fire0SpreadCurve, fire0SpreadCooldown;
    protected float fire0Spread;

    // Secondary fire
    public GameObject fire1Projectile = null;
    public int fire1Damage = 0;
    public int fire1DamageRandomRange = 0;
    public int fire1CurrentAmmo = 0;
    public int fire1AmmoMax = 0;
    public int fire1AmmoPerClip = -1;
    public float fire1MaxSpread = 0;
    public AnimationCurve fire1SpreadCurve, fire1SpreadCooldown;
    protected float fire1Spread;

    public Transform firePoint;
    public Transform grabPoint;

    protected FPSController holder; // Do we want enemies to utilize the weapon base for their weapons?
    new protected Rigidbody rigidbody;

    protected LayerMask weaponLayer;
    protected LayerMask weaponHeldLayer;

    // Note: for detecting enemies in hitscan fire code, avoid layers! Use an enemy component check instead.
    // A layer check can be restricting (what if we want an enemy on a different layer for something else?).

    /// <summary>
    /// Primary fire of this weapon. Use it to initiate a damaging raycast, spawn a projectile prefab, perform a melee animation, etc.
    /// </summary>
    public abstract void FirePrimary();

    /// <summary>
    /// Secondary fire of this weapon. Fill in to provide functionality, or leave empty to disable secondary fire for this weapon.
    /// </summary>
    public abstract void FireSecondary();

    /// <summary>
    /// Request damage amount from this weapon.
    /// </summary>
    /// <param name="fireMode">Specifies which fire mode - primary (0) or secondary (1). Leaves open the possibility for more fire modes.</param>
    /// <param name="rng">Add a value to the base damage in the range of [-damageRandomRange, damageRandomRange).</param>
    public virtual int GetDamage(int fireMode, bool rng) {
        switch (fireMode) {
            case 0:
                return fire0Damage + (rng ? Random.Range(-fire0DamageRandomRange, fire0DamageRandomRange) : 0);
            case 1:
                return fire1Damage + (rng ? Random.Range(-fire1DamageRandomRange, fire1DamageRandomRange) : 0);
            default:
                return fire0Damage + (rng ? Random.Range(-fire0DamageRandomRange, fire0DamageRandomRange) : 0);
        }
    }

    /// <summary>
    /// Move the weapon to the player's weapon grab point, turn on kinematic and assign layer to held.
    /// </summary>
    public virtual void PickUpWeapon(FPSController player) {
        holder = player;
        transform.SetParent(player.grabPoint.parent);

        transform.localRotation = Quaternion.identity;
        transform.position = player.grabPoint.position;
        transform.position += transform.position - grabPoint.position;

        gameObject.layer = weaponHeldLayer;
        foreach (Transform child in transform) {
            child.gameObject.layer = weaponHeldLayer;
        }

        rigidbody.isKinematic = true;
        heldByPlayer = true;
    }

    /// <summary>
    /// Unparent the weapon from the player, turn off kinematic and optionally "throw" the weapon a bit.
    /// </summary>
    public virtual void DropWeapon(FPSController player, bool throwWeapon) {
        holder = null;
        transform.parent = null;

        gameObject.layer = weaponLayer;
        foreach (Transform child in transform) {
            child.gameObject.layer = weaponLayer;
        }

        rigidbody.isKinematic = false;
        heldByPlayer = false;

        if (throwWeapon) {
            Vector3 forceDir = player.attachedGunCamera.transform.forward;
            Vector3 forceRot = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
            
            // hard code is best code
            rigidbody.AddForce(forceDir * Random.Range(150, 300));
            rigidbody.AddTorque(forceRot * Random.Range(100, 250));
        }
    }

    protected virtual void Awake() {
        rigidbody = GetComponent<Rigidbody>();

        weaponLayer = LayerMask.NameToLayer("Weapon");
        weaponHeldLayer = LayerMask.NameToLayer("WeaponHeld");
    }

    protected virtual void Update() {
        // Automatically decrease weapon spread
        fire0Spread = Mathf.Clamp01(fire0Spread - fire0SpreadCooldown.Evaluate(fire0Spread) * Time.deltaTime);
        fire1Spread = Mathf.Clamp01(fire1Spread - fire1SpreadCooldown.Evaluate(fire1Spread) * Time.deltaTime);
    }

    /// <summary>
    /// Returns an euler direction that represents the path a hitscan or projectile should take if fired from a weapon, rather than from the camera.
    /// 
    /// This ensures that despite the origin offset, a projectile will still hit what the player is looking at.
    /// </summary>
    /// <param name="eyes">The camera (player) view point position</param>
    /// <param name="aim">The euler direction the player is looking</param>
    /// <param name="distance">The max distance the raycast can go</param>
    /// <param name="gunPos">The position the gun will fire from</param>
    /// <param name="mask">Raycast layer mask</param>
    public static Vector3 CalcDirForFiringFromGun(Vector3 eyes, Vector3 aim, float distance, Vector3 gunPos, LayerMask mask) {
        RaycastHit hit;
        if (Physics.Raycast(eyes, aim, out hit, mask)) {
            return (gunPos - hit.point).normalized;
        } else {
            return aim;
        }
    }

    /// <summary>
    /// Returns a direction vector that is a randomly-affected version of the input direction.
    /// 
    /// Tweak the weapon spread values to change how spread affects the output.
    /// </summary>
    /// <param name="fireMode">Specifies which fire mode - primary (0) or secondary (1). Leaves open the possibility for more fire modes.</param>
    public virtual Vector3 AddSpreadToDirection(int fireMode, Vector3 direction) {
        // rand acts as radius, where spread is the max and spreadAmount is 0-1 scale
        float rand = 0;
        if (fireMode == 0) {
            rand = fire0MaxSpread * fire0Spread;
        } else if (fireMode == 1) {
            rand = fire1MaxSpread * fire1Spread;
        }

        // In euler,
        Vector3 spreadAdd = new Vector3(Random.Range(-rand, rand), Random.Range(-rand, rand), Random.Range(-rand, rand));

        // I just want to add euler angles to the rotation ffs what is this
        Quaternion fuckQuaternions = Quaternion.FromToRotation(Vector3.up, direction) * Quaternion.Euler(spreadAdd);

        return fuckQuaternions * Vector3.up;
    }

    /// <summary>
    /// Increases the fire spread value by the corresponding value on the spread curve (current spread amount is the input).
    /// </summary>
    /// <param name="fireMode">Specifies which fire mode - primary (0) or secondary (1). Leaves open the possibility for more fire modes.</param>
    public virtual void IncreaseSpreadInstant(int fireMode) {
        if (fireMode == 0) {
            fire0Spread += fire0SpreadCurve.Evaluate(fire0Spread);
        } else if (fireMode == 1) {
            fire1Spread += fire1SpreadCurve.Evaluate(fire1Spread);
        }
    }
}
