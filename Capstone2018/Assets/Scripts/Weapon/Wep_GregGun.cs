﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wep_GregGun : WeaponBase {

    // Primary is a simple raycast + spread
    public override void FirePrimary() {
        // Fire from player view if held, else fire from the muzzle
        Transform fire = heldByPlayer ? holder.attachedCamera.transform : firePoint;

        // Ray is forward plus some spread amount
        Ray ray = new Ray(fire.position, AddSpreadToDirection(0, fire.forward));

        // Ignore these layers (weapon's own collider and player collider)
        int mask = ~((1 << LayerMask.NameToLayer("WeaponHeld")) | ( 1 << LayerMask.NameToLayer("Player")));

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Constants.WEP_MAX_RAY_DIST, mask)) {
            GLDraw.DrawLine(firePoint.position, hit.point, Color.red, Time.deltaTime * 60);
        } else {
            GLDraw.DrawLine(firePoint.position, ray.direction * Constants.WEP_MAX_RAY_DIST, Color.red, Time.deltaTime * 60);
        }

        // Apply spread AFTER fire so our first shot can be dead on
        IncreaseSpreadInstant(0);
    }

    // Empty
    public override void FireSecondary() { }

    // Pickup override - rotate the gun a smidge
    public override void PickUpWeapon(FPSController player) {
        base.PickUpWeapon(player);

        transform.localRotation = Quaternion.Euler(new Vector3(3, 0, 0));
        transform.position = player.grabPoint.position;
        transform.position += transform.position - grabPoint.position;
    }
}
