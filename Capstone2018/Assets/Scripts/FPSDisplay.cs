﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FPSDisplay : MonoBehaviour {
	float deltaTime = 0.0f;
	
	bool average = true;

	const int averageCount = 120;
	List<float> lastFps;

	float averageFps;

	void Awake() {
		if (average) {
			averageFps = Time.deltaTime;
			lastFps = new List<float>();
			for (int i = 0; i < averageCount; ++i)
				lastFps.Add(averageFps);
		}
	}
	void Update() {
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

		if (average) {
			lastFps.RemoveAt(averageCount-1);
			lastFps.Insert(0, Time.deltaTime);

			float accum = 0;
			foreach (float f in lastFps) {
				accum += f;
			}
			averageFps = accum / averageCount;
		}
	}

	void OnGUI() {
		int w = Screen.width, h = Screen.height;

		GUIStyle style = new GUIStyle();

		Rect rect = new Rect(0, 0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 2 / 100;
		style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = Time.timeScale == 0f ? 0f : 1.0f / deltaTime;
		string text = "";
		if (average) {
			text = string.Format("{0:0.0} ms ({1:0.} fps) ({2:0.} avg)", msec, fps, 1.0f / averageFps);
		} else {
			text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
		}
		GUI.Label(rect, text, style);
	}
}