﻿
// A space dedicated to specifying constants and enumerators that are globally accessible

#region ENUMS

public enum WeaponType { Other /*default*/, None, Melee, Hitscan, Projectile };

#endregion
#region CONSTANTS

public class Constants {

    public const float WEP_MAX_RAY_DIST = 1024;
    public const int MAX_PORTALS_PER_FRAME = 4;
}

#endregion