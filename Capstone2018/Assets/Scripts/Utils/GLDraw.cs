﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GLDraw : MonoBehaviour {

    static GLDraw instance;

    // To hold onto lines to be drawn this frame, and lines to get removed after loop
    static List<List<object>> lines;
    static List<List<object>> toRemove;

    Material lineMaterial;

    void Awake() {
        // Set this as the only GLDraw. If another comes along, it dies
        if (instance == null)
            instance = this;
        else
            Destroy(this);
        
        Destroy(lineMaterial);
    }

    // Rendering
    void OnRenderObject() {
        DrawLines();
    }
    // void OnDrawGizmos() {
    //     DrawLines();
    // }

    void DrawLines() {
        if (lines == null)
            lines = new List<List<object>>();
        if (toRemove == null)
            toRemove = new List<List<object>>();

        if (lineMaterial == null) {
            lineMaterial = new Material(Shader.Find("Hidden/Internal-Colored"));
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
            // Draw over everything
            lineMaterial.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
        }

        if (lines.Count == 0)
            return;

        lineMaterial.SetPass(0);

        // GL.PushMatrix();
        GL.Begin(GL.LINES);

        for (int i = 0; i < lines.Count; ++i) {
            List<object> objs = lines[i];

            float time = (float) objs[0];
            Vector3 start = (Vector3) objs[1];
            Vector3 end = (Vector3) objs[2];
            Color color = (Color) objs[3];

            if (Time.time >= time) {
                toRemove.Add(objs);
            }

            GL.Color(color);
            GL.Vertex3(start.x, start.y, start.z);
            GL.Vertex3(end.x, end.y, end.z);
        }

        foreach (List<object> remove in toRemove) {
            lines.Remove(remove);
        }
        toRemove.Clear();

        GL.End();
        // GL.PopMatrix();
    }

    /// Static funcions ///

    /// <summary>
    /// Schedules a line to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// </summary>
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float time) {
        // if (instance == null)
        //     return;
        if (instance == null) {
            // instance = Camera.main.gameObject.GetComponent<GLDraw>();
            // if (instance == null) {
                // Force a GLDraw to be added to the main camera, since we want to use it in this scene
                instance = (GLDraw) Camera.main.gameObject.AddComponent(typeof(GLDraw));
            // }
        }
        if (lines == null)
            lines = new List<List<object>>();

        lines.Add(new List<object>(new object[] {Time.time + time, start, end, color}));
    }

    /// <summary>
    /// Schedules a line to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// 
    /// Line lasts for one frame.
    /// </summary>
    public static void DrawLine(Vector3 start, Vector3 end, Color color) {
        DrawLine(start, end, color, 0.001f);
    }

    /// <summary>
    /// Schedules a line to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// 
    /// Line will be colored white and lasts for one frame.
    /// </summary>
    public static void DrawLine(Vector3 start, Vector3 end) {
        DrawLine(start, end, Color.white, 0.001f);
    }

    /// Schedules a ray to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// </summary>
    public static void DrawRay(Vector3 start, Vector3 ray, Color color, float time) {
        DrawLine(start, start + ray, color, time);
    }

    /// Schedules a ray to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// 
    /// Ray lasts for one frame.
    /// </summary>
    public static void DrawRay(Vector3 start, Vector3 ray, Color color) {
        DrawLine(start, start + ray, color, 0.001f);
    }

    /// Schedules a ray to be drawn using GL functions rather than Debug so it may be seen in-game.
    /// 
    /// Ray will be colored white and lasts for one frame.
    /// </summary>
    public static void DrawRay(Vector3 start, Vector3 ray) {
        DrawLine(start, start + ray, Color.white, 0.001f);
    }
}
