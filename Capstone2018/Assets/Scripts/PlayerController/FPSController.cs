﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A lot of cues taken from the SuperCharacterController's methodology
// https://roystanross.wordpress.com/2015/05/10/custom-character-controller-in-unity-part-6-ground-detection/

// And this article/blog thingy
// https://mariusgames.com/3d-character-controller-db7cd3a7b4df

public class FPSController : MonoBehaviour {

    [Header("Movement")]
    public float movementSpeed;
    public float runSpeed;

    // Physics stuff
    public float gravityScale;
    public float terminalVelocityY;

    public float runLerpTime;
    public float jumpVelocity;
    public float midairReleaseVelocity;

    public float maxWallAngle;

    float actualMovementSpeed;
    float runLerpCurrent;

    bool grounded;

    bool didDoubleJump;

    Vector3 velocity;

    [Header("Camera")]
    public Camera attachedCamera;
    public Camera attachedGunCamera;
    public float normalFov, runFov;
    public float mouseSensitivity;

    Vector2 mouseAngle;
    float camRotationX;

    [Header("Collision")]
    public int terrainCollisionSamplesSquared;
    public int groundCollisionLoops; // Total amt of meshes to check per frame (default to 4)
    public float lastNormalFixedInterpolation; // Best to lerp the last normal Y than immediate set
    Vector3 lastStoodOnNormal, actualLastNormal; // Take into account current standing normal to make slope walking work properly

    [Header("Weapon")]
    public float weaponGrabRange;
    public Transform grabPoint;
    public WeaponBase weaponHeld;

    [Header("Misc")]
    public bool showDebugLines;

    public LayerMask collidingLayers;
    public SphereCollider groundSphere, midSphere, headSphere;

    void Awake() {
        lastStoodOnNormal = Vector3.up;
        actualLastNormal = Vector3.up;
        actualMovementSpeed = movementSpeed;
        // Cursor.lockState = CursorLockMode.Locked;

        // To stand within 0.0001 units of a portal frame (which isn't hard to do with our units), we have to manually override the
        // camera near clip plane here (inspector won't allow it to drop past 0.01 units for depth buffer accuracy purposes)
        attachedCamera.nearClipPlane = 0.001f;
    }

    void Update() {
        #region Camera input & rotation

        // Track up/down and clamp to prevent looking upside-down+backwards
        camRotationX += Input.GetAxis("Mouse Y") * mouseSensitivity;
        camRotationX = Mathf.Clamp(camRotationX, -90, 90);
        attachedCamera.transform.localEulerAngles = Quaternion.Euler(-camRotationX, 0, 0).eulerAngles;

        // Left/right
        transform.Rotate(0f, Input.GetAxis("Mouse X") * mouseSensitivity, 0f);

        #endregion
        #region Movement (input, speed, rot)

        // Movement axes (WASD, joystick, etc.) with speed included
        Vector2 inputs = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        // Adjust, clamp run lerp
        runLerpCurrent += (Input.GetButton("Run") && inputs != Vector2.zero) ? Time.deltaTime : -Time.deltaTime;
        runLerpCurrent = Mathf.Clamp(runLerpCurrent, 0, runLerpTime);

        actualMovementSpeed = Mathf.Lerp(movementSpeed, runSpeed, runLerpCurrent / runLerpTime);

        // FOV run effect
        attachedCamera.fieldOfView = Mathf.Lerp(normalFov, runFov, runLerpCurrent / runLerpTime);
        attachedGunCamera.fieldOfView = attachedCamera.fieldOfView;

        // Scale input by speed
        inputs = inputs.normalized * actualMovementSpeed;

        // Movement translated into 3D w/ camera angle, before slope
        Vector3 rotatedInputs = transform.forward * inputs.y + transform.right * inputs.x;

        // Get slope right and up
        Vector3 right = new Vector3(lastStoodOnNormal.y, -lastStoodOnNormal.x, 0f);
        Vector3 forward = new Vector3(0f, -lastStoodOnNormal.z, lastStoodOnNormal.y);

        // Factor in slope for final movement translation
        Vector3 desiredMove = forward.normalized * rotatedInputs.z + right.normalized * rotatedInputs.x;

        if (showDebugLines) {
            // Debug -- draw current direction & magnitude of movement
            Vector3 spot = new Vector3(groundSphere.transform.position.x, groundSphere.transform.position.y - groundSphere.radius, groundSphere.transform.position.z);
            GLDraw.DrawRay(spot, desiredMove, Color.green);
        }

        #endregion
        #region Jump

        float jump = 0f;

        if (Input.GetButtonDown("Jump")) {
            if (grounded || !didDoubleJump)
                jump = jumpVelocity;
            if (!grounded && !didDoubleJump)
                didDoubleJump = true;
        }

        if (!grounded && Input.GetButtonUp("Jump")) {
            if (velocity.y > midairReleaseVelocity)
                velocity.y = midairReleaseVelocity;
        }

        // elegance is my middle name
        if (jump > 0) {
            grounded = false;
            velocity.y = jump;
        }

        #endregion
        #region Gravity, Velocity and Movement

        // Apply input and jumping into velocity (DO SMOOTHING LATER)
        if (grounded) {
            velocity = new Vector3(desiredMove.x, desiredMove.y, desiredMove.z);
        } else {
            velocity = new Vector3(desiredMove.x, velocity.y, desiredMove.z);
        }

        // Gravity
        if (!grounded) {
            velocity.y += Physics.gravity.y*gravityScale*Time.deltaTime;
            actualLastNormal = Vector3.up;
        }

        // Terminal velocity
        if (velocity.y < terminalVelocityY)
            velocity.y = terminalVelocityY;

        // Move position by velocity (collision resolution comes right after!)
        transform.position += velocity * Time.deltaTime;

        #endregion
        #region Ground Collisions (incl. grounded business)

        int loop = 0;

        List<Collider> solvedCollisions = new List<Collider>();

        float largestAngleHit = 0f;
        Vector3 largestNormal = Vector3.up;

        do {
            // Ground collisions
            Collider[] groundCols = Physics.OverlapSphere(
                new Vector3(transform.position.x, transform.position.y + groundSphere.transform.localPosition.y, transform.position.z),
                groundSphere.radius + 0.001f, collidingLayers.value);

            if (groundCols.Length > 0) {
                // Sort for closest colliders
                System.Array.Sort(groundCols,
                            delegate (Collider col1, Collider col2) {
                                return Vector2.Distance(groundSphere.transform.position, col1.transform.position).CompareTo(
                                    Vector2.Distance(groundSphere.transform.position, col2.transform.position));
                            });

                Collider closest = null;
                foreach (Collider col in groundCols) {
                    if (!solvedCollisions.Contains(col)) {
                        solvedCollisions.Add(col);
                        closest = col;
                        break;
                    }
                }
                if (closest == null) // Resolved all collisions on this frame (as best we could)
                    break;

                Vector3 closestPoint = Vector3.zero;

                // MeshColliders are different than the other collider types
                if (closest is MeshCollider) {
                    // Add BSPTree to MeshCollider if it doesn't already have one
                    BSPTree tree = closest.GetComponent<BSPTree>();
                    if (!tree)
                        tree = closest.gameObject.AddComponent<BSPTree>();

                    closestPoint = tree.ClosestPointOn(groundSphere.transform.position, groundSphere.radius);
                } else if (closest is TerrainCollider) {
                    TerrainCollider tc = (TerrainCollider) closest;
                    TerrainData tcd = tc.terrainData;

                    Vector3 point = Vector3.zero;
                    float dst = Mathf.Infinity;
                    for (int y = 0; y < terrainCollisionSamplesSquared; ++y) {
                        for (int x = 0; x < terrainCollisionSamplesSquared; ++x) {
                            Vector3 p = new Vector3(
                                    groundSphere.transform.position.x + ((float)x / terrainCollisionSamplesSquared)*groundSphere.radius - groundSphere.radius/2f,
                                    groundSphere.transform.position.y,
                                    groundSphere.transform.position.z + ((float)y / terrainCollisionSamplesSquared)*groundSphere.radius - groundSphere.radius/2f);
                            
                            p.y = closest.transform.position.y + closest.gameObject.GetComponent<Terrain>().SampleHeight(p);

                            float d = Vector3.Distance(groundSphere.transform.position, p);
                            if (d < dst) {
                                dst = d;
                                point = p;
                            }
                        }
                    }
                    if (dst < Mathf.Infinity) {
                        closestPoint = point;
                    } else {
                        throw new System.ArithmeticException("Terrain point not found");
                    }
                } else {
                    closestPoint = closest.ClosestPoint(groundSphere.transform.position);
                }

                Vector3 currentOffset = groundSphere.transform.position - closestPoint;
                Vector3 unitOffset = currentOffset.normalized; // normal

                //actualLastNormal = unitOffset;

                // Current (interpolated) normal
                if (showDebugLines)
                    GLDraw.DrawLine(closestPoint, closestPoint + unitOffset, Color.red);

                // Get normal of currently-hitting surface (different from interpolated normal on seams and edges)
                RaycastHit hit;
                Ray ray = new Ray(closestPoint + unitOffset, -unitOffset);

                Physics.Raycast(ray, out hit, 1.01f, collidingLayers);

                if (hit.collider == null) {
                    Debug.LogError("Collider raycast turned up empty! Something's wrong here.");
                    return;
                }

                if (showDebugLines)
                    GLDraw.DrawLine(hit.point, hit.point + hit.normal, Color.cyan);

                // Calc amount needed to resolve collision
                Vector3 offsetNeeded = (groundSphere.radius - currentOffset.magnitude) * unitOffset;

                transform.position = new Vector3(transform.position.x + offsetNeeded.x, transform.position.y + offsetNeeded.y, transform.position.z + offsetNeeded.z);

                // Determine, using unitOffset (normal), if we're standing on a "wall" or a "ground"
                float normalAngle = Mathf.Rad2Deg * Mathf.Asin(unitOffset.y);

                if (normalAngle > largestAngleHit) {
                    largestAngleHit = normalAngle;
                    largestNormal = unitOffset;
                }
            } else {
                grounded = false;
                break;
            }
        } while (loop++ < groundCollisionLoops);

        // In all collisions, take the one that most resembles a flat floor for wall/ground as what determines grounded
        if (largestAngleHit < 90 - maxWallAngle) {
            grounded = false;
            actualLastNormal = Vector3.up; // It was a wall, just consider it as up so our frame of reference for movement isn't the wall normal.
        } else {
            velocity.y = 0f;
            grounded = true;
            didDoubleJump = false;
            actualLastNormal = largestNormal;
        }

        lastStoodOnNormal = Vector3.Lerp(lastStoodOnNormal, actualLastNormal, lastNormalFixedInterpolation);

        #endregion
        #region Mid Collisions

        Collider[] midCols = Physics.OverlapSphere(
            new Vector3(transform.position.x, transform.position.y + midSphere.transform.localPosition.y, transform.position.z),
            midSphere.radius + 0.001f, collidingLayers.value);
        
        foreach (Collider col in midCols) {
            Vector3 closestPoint = Vector3.zero;

            if (col is MeshCollider) {
                // Add BSPTree to MeshCollider if it doesn't already have one
                BSPTree tree = col.GetComponent<BSPTree>();
                if (!tree)
                    tree = col.gameObject.AddComponent<BSPTree>();

                closestPoint = tree.ClosestPointOn(midSphere.transform.position, midSphere.radius);
            } else if (col is TerrainCollider) {
                TerrainCollider tc = (TerrainCollider) col;
                closestPoint = tc.ClosestPointOnBounds(midSphere.transform.position);
            } else {
                closestPoint = col.ClosestPoint(midSphere.transform.position);
            }

            Vector3 currentOffset = midSphere.transform.position - closestPoint;
            Vector3 unitOffset = currentOffset.normalized; // normal

            // Current (interpolated) normal
            if (showDebugLines)
                GLDraw.DrawLine(closestPoint, closestPoint + unitOffset, Color.red);

            // Get normal of currently-hitting surface (different from interpolated normal on seams and edges)
            RaycastHit hit;
            Ray ray = new Ray(closestPoint + unitOffset, -unitOffset);

            Physics.Raycast(ray, out hit, 1.01f, collidingLayers);

            if (hit.collider == null) {
                Debug.LogError("Collider raycast turned up empty! Something's wrong here.");
                return;
            }

            if (showDebugLines)
                GLDraw.DrawLine(hit.point, hit.point + hit.normal, Color.cyan);

            // Calc amount needed to resolve collision
            Vector3 offsetNeeded = (midSphere.radius - currentOffset.magnitude) * unitOffset;

            transform.position = new Vector3(transform.position.x + offsetNeeded.x, transform.position.y + offsetNeeded.y, transform.position.z + offsetNeeded.z);
        }

        #endregion
        #region Head Collisions

        Collider[] headCols = Physics.OverlapSphere(
            new Vector3(transform.position.x, transform.position.y + headSphere.transform.localPosition.y, transform.position.z),
            headSphere.radius + 0.001f, collidingLayers.value);
        
        foreach (Collider col in headCols) {
            Vector3 closestPoint = Vector3.zero;

            if (col is MeshCollider) {
                // Add BSPTree to MeshCollider if it doesn't already have one
                BSPTree tree = col.GetComponent<BSPTree>();
                if (!tree)
                    tree = col.gameObject.AddComponent<BSPTree>();

                closestPoint = tree.ClosestPointOn(headSphere.transform.position, headSphere.radius);
            } else if (col is TerrainCollider) {
                TerrainCollider tc = (TerrainCollider) col;
                closestPoint = tc.ClosestPointOnBounds(headSphere.transform.position);
            } else {
                closestPoint = col.ClosestPoint(headSphere.transform.position);
            }

            Vector3 currentOffset = headSphere.transform.position - closestPoint;
            Vector3 unitOffset = currentOffset.normalized; // normal

            // Current (interpolated) normal
            if (showDebugLines)
                GLDraw.DrawLine(closestPoint, closestPoint + unitOffset, Color.red);

            // Get normal of currently-hitting surface (different from interpolated normal on seams and edges)
            RaycastHit hit;
            Ray ray = new Ray(closestPoint + unitOffset, -unitOffset);

            Physics.Raycast(ray, out hit, 1.01f, collidingLayers);

            if (hit.collider == null) {
                Debug.LogError("Collider raycast turned up empty! Something's wrong here.");
                return;
            }

            if (showDebugLines)
                GLDraw.DrawLine(hit.point, hit.point + hit.normal, Color.cyan);

            // Calc amount needed to resolve collision
            Vector3 offsetNeeded = (headSphere.radius - currentOffset.magnitude) * unitOffset;
            
            transform.position = new Vector3(transform.position.x + offsetNeeded.x, transform.position.y + offsetNeeded.y, transform.position.z + offsetNeeded.z);

            float normalAngle = Mathf.Rad2Deg * Mathf.Asin(unitOffset.y);

            // Head go bonk. Drop velocity y based on normal angle so eg, -0.001 angle
            // hardly affects velocity at all but -1 angle is a perfect ceiling stop
            if (normalAngle < 0 && velocity.y > 0) {
                velocity.y *= (1 + unitOffset.y);
            }
        }
        #endregion
        #region Weapon Pickup, Drop, Firing

        RaycastHit weaponHit;
        if (Input.GetKeyDown(KeyCode.E) && Physics.SphereCast(attachedCamera.transform.position, 0.1f, attachedCamera.transform.forward, out weaponHit, weaponGrabRange, 1 << LayerMask.NameToLayer("Weapon"))) {
            WeaponBase wep = weaponHit.collider.attachedRigidbody.gameObject.GetComponent<WeaponBase>();

            weaponHeld = wep;

            wep.PickUpWeapon(this);
        }

        if (weaponHeld != null && Input.GetKeyDown(KeyCode.T)) {
            weaponHeld.DropWeapon(this, true);
            weaponHeld = null;
        }

        if (weaponHeld != null && Input.GetMouseButtonDown(0)) {
            weaponHeld.FirePrimary();
        }

        if (weaponHeld != null && Input.GetMouseButtonDown(1)) {
            weaponHeld.FireSecondary();
        }
        #endregion
    }
}
