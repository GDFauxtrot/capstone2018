﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshFollowPoint : MonoBehaviour {

    public Transform destination;
    public LayerMask blocksVision;

    private Animator animator;

    //Nav Mesh Agent Settings
    private NavMeshAgent navMeshAgent;
    [SerializeField]
    private float minFollowDistance, maxVisionDistance;

	void Start () {
        animator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit raycastHit;
        Physics.Raycast(transform.position, destination.position - transform.position, out raycastHit, maxVisionDistance, blocksVision);

        Vector3 move = transform.InverseTransformDirection(navMeshAgent.desiredVelocity.normalized);
        move = Vector3.ProjectOnPlane(move, Vector3.up);

        animator.SetFloat("Forward", navMeshAgent.desiredVelocity.magnitude / navMeshAgent.speed, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", Mathf.Atan2(move.x, move.z) / (Mathf.PI * 2), 0.1f, Time.deltaTime);

        //Delete this, testing only *******************
        animator.SetBool("Crouch", Input.GetKey(KeyCode.C));

        if (raycastHit.collider)
        {
            if(raycastHit.collider.CompareTag(destination.tag) && Vector3.Distance(destination.position, transform.position) <= minFollowDistance)
            {
                navMeshAgent.ResetPath();
                
                transform.LookAt(destination, Vector3.up);
                transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
                animator.SetFloat("Turn", transform.rotation.eulerAngles.y / 180f, 0.1f, Time.deltaTime);
            }
            else
            {
                navMeshAgent.SetDestination(destination.position);
            }
        }
	}
}
