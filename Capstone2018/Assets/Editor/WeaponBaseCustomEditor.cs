﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Reflection;

[CustomEditor(typeof(WeaponBase), true)]
[CanEditMultipleObjects]
public class WeaponBaseCustomEditor : Editor {
    
    bool primaryUnfolded = true, secondaryUnfolded = true;

    SerializedProperty weaponName, weaponTypePrimary, weaponTypeSecondary;
    SerializedProperty fire0Projectile, fire0Damage, fire0DamageRandomRange, fire0CurrentAmmo, fire0AmmoMax, fire0AmmoPerClip;
    SerializedProperty fire0MaxSpread, fire0SpreadCurve, fire0SpreadCooldown;
    SerializedProperty fire1Projectile, fire1Damage, fire1DamageRandomRange, fire1CurrentAmmo, fire1AmmoMax, fire1AmmoPerClip;
    SerializedProperty fire1MaxSpread, fire1SpreadCurve, fire1SpreadCooldown;
    SerializedProperty firePoint, grabPoint;

    FieldInfo[] publicFields;
    string[] publicFieldNames;

    void OnEnable() {
        // fun fun fUN FUN FUN FUN THIS IS FUN
        weaponName = serializedObject.FindProperty("weaponName");
        weaponTypePrimary = serializedObject.FindProperty("weaponTypePrimary");
        weaponTypeSecondary = serializedObject.FindProperty("weaponTypeSecondary");
        fire0Projectile = serializedObject.FindProperty("fire0Projectile");
        fire0Damage = serializedObject.FindProperty("fire0Damage");
        fire0DamageRandomRange = serializedObject.FindProperty("fire0DamageRandomRange");
        fire0CurrentAmmo = serializedObject.FindProperty("fire0CurrentAmmo");
        fire0AmmoMax = serializedObject.FindProperty("fire0AmmoMax");
        fire0AmmoPerClip = serializedObject.FindProperty("fire0AmmoPerClip");
        fire0MaxSpread = serializedObject.FindProperty("fire0MaxSpread");
        fire0SpreadCurve = serializedObject.FindProperty("fire0SpreadCurve");
        fire0SpreadCooldown = serializedObject.FindProperty("fire0SpreadCooldown");

        fire1Projectile = serializedObject.FindProperty("fire1Projectile");
        fire1Damage = serializedObject.FindProperty("fire1Damage");
        fire1DamageRandomRange = serializedObject.FindProperty("fire1DamageRandomRange");
        fire1CurrentAmmo = serializedObject.FindProperty("fire1CurrentAmmo");
        fire1AmmoMax = serializedObject.FindProperty("fire1AmmoMax");
        fire1AmmoPerClip = serializedObject.FindProperty("fire1AmmoPerClip");
        fire1MaxSpread = serializedObject.FindProperty("fire1MaxSpread");
        fire1SpreadCurve = serializedObject.FindProperty("fire1SpreadCurve");
        fire1SpreadCooldown = serializedObject.FindProperty("fire1SpreadCooldown");

        firePoint = serializedObject.FindProperty("firePoint");
        grabPoint = serializedObject.FindProperty("grabPoint");
        // THIS IS REQUIRED TO DO, GOOD GOD

        publicFields = typeof(WeaponBase).GetFields(BindingFlags.Instance | BindingFlags.Public);
        publicFieldNames = new string[publicFields.Length+1];
        for (int i = 0; i < publicFields.Length; ++i)
            publicFieldNames[i] = publicFields[i].Name;
        publicFieldNames[publicFields.Length] = "m_Script";
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        EditorGUILayout.PropertyField(weaponName, new GUIContent("Weapon Name"));
        EditorGUILayout.PropertyField(weaponTypePrimary, new GUIContent("Primary Type"));
        EditorGUILayout.PropertyField(weaponTypeSecondary, new GUIContent("Secondary Type"));

        WeaponType primary = (WeaponType) weaponTypePrimary.enumValueIndex;
        WeaponType secondary = (WeaponType) weaponTypeSecondary.enumValueIndex;

        GUIStyle foldoutStyle = new GUIStyle(EditorStyles.foldout);
        foldoutStyle.fontStyle = FontStyle.Bold;
    
        EditorGUILayout.Space();

        primaryUnfolded = EditorGUILayout.Foldout(primaryUnfolded, "Primary Fire", foldoutStyle);

        if (primaryUnfolded && primary != WeaponType.None) {
            // Projectile fire object
            if (primary == WeaponType.Projectile || primary == WeaponType.Other) {
                EditorGUILayout.PropertyField(fire0Projectile, new GUIContent("Projectile"));
            }
            
            // Damage
            EditorGUILayout.PropertyField(fire0Damage, new GUIContent("Damage"));
            EditorGUILayout.PropertyField(fire0DamageRandomRange, new GUIContent("Damage (+/-) RNG Range"));
            
            // Ammunition
            if (primary != WeaponType.Melee) {
                EditorGUILayout.PropertyField(fire0CurrentAmmo, new GUIContent("Current Ammo"));
                EditorGUILayout.PropertyField(fire0AmmoMax, new GUIContent("Max Ammo"));
                EditorGUILayout.PropertyField(fire0AmmoPerClip, new GUIContent("Ammo Per Clip"));
            }

            // Spread
            if (primary != WeaponType.Melee) {
                EditorGUILayout.PropertyField(fire0MaxSpread, new GUIContent("Max Spread"));
                EditorGUILayout.PropertyField(fire0SpreadCurve, new GUIContent("Spread Curve"));
                EditorGUILayout.PropertyField(fire0SpreadCooldown, new GUIContent("Spread Dropoff"));
            }
        }

        EditorGUILayout.Space();

        secondaryUnfolded = EditorGUILayout.Foldout(secondaryUnfolded, "Secondary Fire", foldoutStyle);

        if (secondaryUnfolded && secondary != WeaponType.None) {
            // Projectile fire object
            if (secondary == WeaponType.Projectile || secondary == WeaponType.Other) {
                EditorGUILayout.PropertyField(fire1Projectile, new GUIContent("Projectile"));
            }

            // Damage
            EditorGUILayout.PropertyField(fire1Damage, new GUIContent("Damage"));
            EditorGUILayout.PropertyField(fire1DamageRandomRange, new GUIContent("Damage (+/-) RNG Range"));
            
            // Ammunition
            if (secondary != WeaponType.Melee) {
                EditorGUILayout.PropertyField(fire1CurrentAmmo, new GUIContent("Current Ammo"));
                EditorGUILayout.PropertyField(fire1AmmoMax, new GUIContent("Max Ammo"));
                EditorGUILayout.PropertyField(fire1AmmoPerClip, new GUIContent("Ammo Per Clip"));
            }

            // Spread
            if (primary != WeaponType.Melee) {
                EditorGUILayout.PropertyField(fire1MaxSpread, new GUIContent("Max Spread"));
                EditorGUILayout.PropertyField(fire1SpreadCurve, new GUIContent("Spread Curve"));
                EditorGUILayout.PropertyField(fire1SpreadCooldown, new GUIContent("Spread Dropoff"));
            }
        }

        EditorGUILayout.Space();

        EditorGUILayout.ObjectField(firePoint, new GUIContent("Fire Point"));
        EditorGUILayout.ObjectField(grabPoint, new GUIContent("Grab Point"));

        EditorGUILayout.Space();

        DrawPropertiesExcluding(serializedObject, publicFieldNames);

        serializedObject.ApplyModifiedProperties();
    }
}
